## 2.0.0
- 解决swiper和photoview 共用时滑动冲突问题

## 2.0.0-rc.0
- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)
- ArkTS新语法适配


## 1.1.1

- 适配DevEco Studio 3.1Beta1及以上版本。
- 适配OpenHarmony SDK API version 9及以上版本。

## 1.1.0

- 名称由PhotoView-ETS修改为PhotoView。
- 旧的包photoview-ets已不维护，请使用新包photoview。


## 1.0.2

- 工程由api8升级到api9

## 1.0.1

- 详细功能：
  1. 支持加载网络图片或Resource或PixelMap图片
  2. 支持缩放功能
  3. 支持平移功能
  4. 支持旋转