/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {PhotoView} from '@ohos/photoview';
import router from '@ohos.router';

let that:ESObject;

@Entry
@Preview
@Component
struct Sample {
  @State editFlag: boolean = false
  @State data: PhotoView.Model = new PhotoView.Model();
  @State rotating: boolean  = false;
  private intervalID: number = 0;
  dialogController: CustomDialogController = new CustomDialogController({
    builder: CustomDialogExample({confirm: this.onAccept }),
    alignment: DialogAlignment.Top,
    offset: { dx: 100, dy: 20 },
    cancel: this.existApp,
    autoCancel: true,
  })

  onAccept(index:number) {
    if (index == 0) {
      that.data.setRotationBy(10)
    } else if (index == 1) {
      that.data.setRotationBy(-10)
    } else if (index == 2) {
      that.intervalID = setInterval(()=>{
        that.data.setRotationBy(1)
      }, 15);
      if (that.intervalID % 2 == 0) {
        clearInterval(that.intervalID-1)
        clearInterval(that.intervalID)
      }
    } else if (index == 3) {
      that.data.setRotationTo(0)
    } else if (index == 4) {
      that.data.setRotationTo(90)
    } else if (index == 5) {
      that.data.setRotationTo(180)
    } else if (index == 6) {
      that.data.setRotationTo(270)
    }
  }

  toogleRotation(): void{
    console.info("photo ro:" + this.rotating)
    if (this.rotating) {
      clearInterval(that.intervalID)
    } else {
      that.intervalID = setInterval(()=>{
        that.data.setRotationBy(1)
      }, 15);
    }
    this.rotating = this.rotating?false:true;
    console.info("photo rotating:" + this.rotating)
  }

  onShow() {
    console.info("onAccept onShow")
  }

  existApp() {
    console.info('Click the callback in the blank area')
  }

  build() {
    Stack({ alignContent: Alignment.Top }) {
      PhotoView({ model: this.data })
      Row({ space: 5 }) {
        Image($r('app.media.back'))
          .width(45)
          .height(45)
          .margin({ left: 5 })
          .onClick((event: ClickEvent) => {
            router.back()
          })
        Text('Rotation Sample')
          .width('75%')
          .fontColor(0xffffff)
          .fontSize(20)
          .margin({ top: 10 })
          .layoutWeight(1)
        Image($r('app.media.more'))
          .width(45)
          .height(45)
          .margin({ right: 5 })
          .onClick((event: ClickEvent) => {
            this.dialogController.open()
          })

      }.width('100%').height(80).backgroundColor(0x3d3d3d)
    }
    .height('100%')
    .width('100%')
    .backgroundColor(0x3d3d3d)
  }

  aboutToAppear() {
    that = this;
    this.data
      .setImageResource($r('app.media.wallpaper'))
  }
}

@CustomDialog
struct CustomDialogExample {
  controller: CustomDialogController= new CustomDialogController({builder:()=>{}});
  @State counter: number = 0;
  confirm: ESObject|null = null;

  build() {
    Column() {
      Text("Rotate 10° Right")
        .width('100%')
        .height(60)
        .padding({ left: 20 })
        .fontSize(14)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
         if(!!this.controller) this.controller.close()
          this.confirm(0)
        })
      Text("Rotate 10° Left")
        .width('100%')
        .padding({ left: 20 })
        .fontSize(14)
        .height(60)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
          if(!!this.controller) this.controller.close()
          this.confirm(1)
        })
      Text("Toggle Automatic Rotation")
        .width('100%')
        .height(60)
        .padding({ left: 20 })
        .fontSize(14)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
          if(!!this.controller) this.controller.close()
          this.confirm(2)
        })
      Text('Reset to 0')
        .width('100%')
        .height(60)
        .padding({ left: 20 })
        .fontSize(14)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
          if(!!this.controller)  this.controller.close()
          this.confirm(3)
        })
      Text('Reset to 90')
        .width('100%')
        .height(60)
        .padding({ left: 20 })
        .fontSize(14)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
          if(!!this.controller) this.controller.close()
          this.confirm(4)
        })
      Text('Reset to 180')
        .width('100%')
        .height(60)
        .padding({ left: 20 })
        .fontSize(14)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
          if(!!this.controller) this.controller.close()
          this.confirm(5)
        })
      Text('Reset to 270')
        .width('100%')
        .height(60)
        .padding({ left: 20 })
        .fontSize(14)
        .fontColor(0xffffff)
        .margin({ top: 10 })
        .onClick(() => {
          if(!!this.controller) this.controller.close()
          this.confirm(6)
        })
    }.backgroundColor(0x3d3d3d)
  }
}